const fs = require('fs');

const calculadora = {};
function generarLog (num1,num2,op){
    fs.writeFileSync('log.txt',num1+' '+op+' '+num2+''+'= '+''+result+ '\n',{flag:'a'});
}

function sumar(num1,num2){
    result = num1 + num2;
    generarLog(num1,num2,'+');
    return result;
}
function restar(num1,num2){
    result = num1 - num2;
    generarLog(num1,num2,'-');
    return result;
}
function dividir(num1,num2){
    result = num1 / num2;
    generarLog(num1,num2,'/');
    return result;
}
function multiplicar(num1,num2){
    result = num1 * num2;    
    generarLog(num1,num2,'*');
    return result;
}

calculadora.sumar=sumar;
calculadora.restar=restar;
calculadora.dividir=dividir;
calculadora.multiplicar=multiplicar;

module.exports = calculadora;